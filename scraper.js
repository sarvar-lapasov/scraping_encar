const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
const { v4: uuidv4 } = require("uuid");
const axios = require("axios");

puppeteer.use(StealthPlugin());

async function getExchangeRate() {
    try {
        const apiKey = "XgnwLH1M6WFtjiOujYl9NDzg5TqYYNlA"; // Replace with your API key
        const response = await axios.get(
            `https://api.exchangerate-api.com/v4/latest/KRW`
        );
        return response.data.rates.RUB;
    } catch (error) {
        console.error("Error fetching exchange rate:", error);
        return null;
    }
}

async function translateText(text, from, to) {
    try {
        const { default: translate } = await import("translate");
        translate.engine = "google";
        translate.key = null; // No API key needed for the free version
        const translatedText = await translate(text, { from, to });
        return translatedText;
    } catch (error) {
        console.error("Error translating text:", error);
        return null;
    }
}

async function extractCarsFromPage(page, translateText) {
    const carsData = await page.$$eval("#sr_photo > li", (cars) => {
        return cars.map((car) => {
            const getText = (selector) =>
                car.querySelector(selector)
                    ? car.querySelector(selector).innerText
                    : null;
            const getImage = (selector) =>
                car.querySelector(selector)
                    ? car.querySelector(selector).src
                    : null;

            const title = getText(".cls");
            const brand = getText(".cls strong");
            const year = getText(".yer");
            const km = getText(".km");
            const detal1 = getText(".dtl");
            const detal2 = getText(".ipt");
            const loc = getText(".lo");
            const price = getText(".prc strong");
            const image = getImage("img");

            const increasedPrice = price
                ? parseFloat(price.replace(",", ""))
                : null;
            const changeKm = km
                ? parseFloat(km.replace("km", "").replace(",", ""))
                : null;

            return {
                brand,
                title,
                year,
                changeKm,
                image,
                increasedPrice,
                detal1,
                detal2,
                loc,
            };
        });
    });
    const translatedCars = await Promise.all(
        carsData.map(async (carData) => {
            const translatedTitle = await translateText(
                carData.title,
                "ko",
                "en"
            );
            const translatedBrand = await translateText(
                carData.brand,
                "ko",
                "en"
            );
            const translatedYear = await translateText(
                carData.year,
                "ko",
                "ru"
            );
            const translatedKm = await translateText(
                carData.changeKm,
                "ko",
                "ru"
            );
            const translatedDetal1 = await translateText(
                carData.detal1,
                "ko",
                "ru"
            );
            const translatedDetal2 = await translateText(
                carData.detal2,
                "ko",
                "ru"
            );
            const translatedLoc = await translateText(carData.loc, "ko", "ru");

            return {
                brand: translatedBrand,
                title: translatedTitle,
                year: translatedYear,
                km: translatedKm,
                image: carData.image,
                price: carData.increasedPrice,
                techs: [translatedDetal1, translatedDetal2, translatedLoc],
            };
        })
    );
    return translatedCars;
}
async function getCars() {
    const exchangeRate = await getExchangeRate();
    if (!exchangeRate) {
        console.error("Failed to fetch exchange rate. Exiting...");
        return;
    }
    const browser = await puppeteer.launch({ headless: true });
    const page = await browser.newPage();

    const urls = [
        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%ED%98%84%EB%8C%80._.(C.ModelGroup.%EC%8F%98%EB%82%98%ED%83%80._.Model.YF%20%EC%8F%98%EB%82%98%ED%83%80.))))%22%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%ED%98%84%EB%8C%80._.(C.ModelGroup.%EC%8F%98%EB%82%98%ED%83%80._.Model.NF%20%EC%8F%98%EB%82%98%ED%83%80.))))%22%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%ED%98%84%EB%8C%80._.(C.ModelGroup.%EC%8F%98%EB%82%98%ED%83%80._.Model.EF%20%EC%8F%98%EB%82%98%ED%83%80.))))%22%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%ED%98%84%EB%8C%80._.(C.ModelGroup.%EC%8F%98%EB%82%98%ED%83%80._.Model.%EC%8F%98%EB%82%98%ED%83%80%20II.))))%22%7D",
        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%EA%B8%B0%EC%95%84._.(C.ModelGroup.K7._.Model.K7%20%ED%94%84%EB%A6%AC%EB%AF%B8%EC%96%B4.))))%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22sort%22%3A%22ModifiedDate%22%2C%22page%22%3A1%2C%22limit%22%3A20%2C%22searchKey%22%3A%22%22%2C%22loginCheck%22%3Afalse%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%EA%B8%B0%EC%95%84._.(C.ModelGroup.K7._.Model.K7%20%ED%94%84%EB%A6%AC%EB%AF%B8%EC%96%B4%20%ED%95%98%EC%9D%B4%EB%B8%8C%EB%A6%AC%EB%93%9C.))))%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22sort%22%3A%22ModifiedDate%22%2C%22page%22%3A1%2C%22limit%22%3A20%2C%22searchKey%22%3A%22%22%2C%22loginCheck%22%3Afalse%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%EA%B8%B0%EC%95%84._.(C.ModelGroup.K7._.Model.%EC%98%AC%20%EB%89%B4%20K7.))))%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22sort%22%3A%22ModifiedDate%22%2C%22page%22%3A1%2C%22limit%22%3A20%2C%22searchKey%22%3A%22%22%2C%22loginCheck%22%3Afalse%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%EA%B8%B0%EC%95%84._.(C.ModelGroup.K7._.Model.%EC%98%AC%20%EB%89%B4%20K7%20%ED%95%98%EC%9D%B4%EB%B8%8C%EB%A6%AC%EB%93%9C.))))%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22sort%22%3A%22ModifiedDate%22%2C%22page%22%3A1%2C%22limit%22%3A20%2C%22searchKey%22%3A%22%22%2C%22loginCheck%22%3Afalse%7D",
        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%EC%A0%9C%EB%84%A4%EC%8B%9C%EC%8A%A4._.(C.ModelGroup.G80._.Model.G80%20(RG3_).))))%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22sort%22%3A%22ModifiedDate%22%2C%22page%22%3A1%2C%22limit%22%3A20%2C%22searchKey%22%3A%22%22%2C%22loginCheck%22%3Afalse%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%EC%A0%9C%EB%84%A4%EC%8B%9C%EC%8A%A4._.(C.ModelGroup.G80._.Model.%EC%9D%BC%EB%A0%89%ED%8A%B8%EB%A6%AC%ED%8C%8C%EC%9D%B4%EB%93%9C%20G80%20(RG3_).))))%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22sort%22%3A%22ModifiedDate%22%2C%22page%22%3A1%2C%22limit%22%3A20%2C%22searchKey%22%3A%22%22%2C%22loginCheck%22%3Afalse%7D",

        "http://www.encar.com/dc/dc_carsearchlist.do?carType=kor#!%7B%22action%22%3A%22(And.Hidden.N._.(C.CarType.Y._.(C.Manufacturer.%EC%A0%9C%EB%84%A4%EC%8B%9C%EC%8A%A4._.(C.ModelGroup.G80._.Model.G80.))))%22%2C%22toggle%22%3A%7B%7D%2C%22layer%22%3A%22%22%2C%22sort%22%3A%22ModifiedDate%22%2C%22page%22%3A1%2C%22limit%22%3A20%2C%22searchKey%22%3A%22%22%2C%22loginCheck%22%3Afalse%7D",
    ];

    const allCars = [];

    for (const url of urls) {
        try {
            await page.goto(url, { timeout: 60000 });
            await page.waitForSelector("#sr_photo > li");
            await page.waitForTimeout(3000);

            const cars = await extractCarsFromPage(page, translateText);
            allCars.push(...cars);
        } catch (error) {
            console.error(`Error fetching data from URL: ${url}`, error);
        }
    }

    const carsWithId = allCars.map((car) => ({ ...car, id: uuidv4() }));

    const uniqueCars = carsWithId.filter((car, index, self) => {
        return (
            index ===
            self.findIndex(
                (c) =>
                    c.brand === car.brand &&
                    c.image === car.image &&
                    c.price === car.price
            )
        );
    });
    const carsWithLessThan70000Km = uniqueCars.filter((car) => car.km < 70000);

    const carsInRuble = carsWithLessThan70000Km.map((car) => {
        const priceInRuble =
            (parseFloat(car.price) * exchangeRate * 1.15).toFixed() * 10000;
        return { ...car, price: priceInRuble };
    });

    await browser.close();
    return carsInRuble;
}

module.exports = {
    getCars,
};
